import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){

		// this is the board 

	Board board = new Board();

	/*

	Tile tile = Tile.BLANK;

	System.out.println(tile.getName());

	System.out.println(board);

	// making a scanner for the user to input

	Scanner reader = new Scanner(System.in);

	System.out.println("Please enter the number of the Row");

	// this will be the row number that user will input

	int row= reader.nextInt();

	System.out.println("Please enter the number of Column");

	// this is the column that user will enter

	int col= reader.nextInt();

	// this will print the board before using placeToken

	System.out.println("This is the board before placeToken: "+board);

	// calling placeToken method 

	board.placeToken(row, col);

	// this will print the board after using placeToken

	System.out.println("This is the board after placeToken: "+board);
*/




	System.out.println("Welcome to this Game!");

	// numCastels will be used as the 5x5 array in Board class

	int numCastles = 7;

	int turns = 0;

	// while numCastles is greter than 0 and turns is less than 8
	// the game will be looping

	while (numCastles >0 && turns <8){

		System.out.println(board);
		System.out.println("The numCastles is: "+ numCastles);
		System.out.println("The number of turn is: "+turns);

		// making a scanner for the user to input

	Scanner reader = new Scanner(System.in);

	System.out.println("Please enter the number of the Row");

	// this will be the row number that user will input

	int row= reader.nextInt();

	System.out.println("Please enter the number of Column");

	// this is the column that user will enter

	int col= reader.nextInt();

	// calling placeToken

	int number = board.placeToken(row, col);

	// if number is less than 0 then try again 

	if (number <0){

		System.out.println("Try again");

	}

	else if (number == 1 ){

		System.out.println("There was a Wall at this position");

		turns++;
	}
	else {

		System.out.println("Castle tile was successfully placed");

		turns++;
		numCastles--;
	}

		}
		System.out.println("The final board is: "+ board);
		if (numCastles == 0){

			System.out.println("You have won");

		}
		else {
			System.out.println("You lost");
		}
	}
}