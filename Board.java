import java.util.Random;
public class Board{

	// these are the fields for board class

	private Tile[][] grid;
	private final int sizeLength;
	private Random rand;

	
	public Board() {
		sizeLength = 5;

		this.rand = new Random();


		
		grid = new Tile[sizeLength][sizeLength];


		
		for (int i = 0; i<grid.length; i++) {
			
			int position= rand.nextInt(grid.length);
			
			for (int j = 0; j<grid[i].length; j++) {
				
				if (j== position){

					grid[i][j] = Tile.HIDDEN_WALL;

				}
				else{
				grid[i][j] = Tile.BLANK;
			}
			}
			
		}
		
	}
	public String toString() {
		
		String rows = "";
		
		for (int i = 0; i<grid.length; i++) {
			
			rows += "\n";
			
			for (int j = 0; j<grid[i].length; j++) {
				rows += (grid[i][j].getName() + "  ");
			}
			
		}
		
		return rows;
		
	}
	
	public int placeToken(int row, int col) {
		
		if( (row <= sizeLength && row >= 0) && (col <= sizeLength && col >= 0) ) {
			
			if (grid[row][col].equals(Tile.CASTLE) || grid[row][col].equals(Tile.WALL)) {

				
				return -1;
			}
			else if (grid[row][col].equals(Tile.HIDDEN_WALL)){

				grid[row][col]=Tile.WALL;

				return 1;

			}
			else{

				grid[row][col]= Tile.CASTLE;

				return 0;
			}
			
		}
		else{

			return -2;
		}
	}
}