public enum Tile{

	// Modifying the Tile enum and its values

	BLANK("_"),
	HIDDEN_WALL("_"),
	WALL("W"),
	CASTLE("C");

	private String name;

	// this is the constractor

	private Tile(String name){

		this.name=name;
	}

	// this will be getting names that user will be puting in. 

	public String getName(){
		return this.name;
	}

}